/**
 * This is solving only the last part of the riddle:
 * If 1/2 of 5 is 3, then what is 1/3 of 10?
 */
fn solve_riddle_1(base: usize, proportion: f32) -> f32 {
  let solution: f32 = (base as f32 * proportion).ceil();
  // println!(
  //   "base is {} | proportion is {} | and result is {}",
  //   base, proportion, &solution
  // );

  solution
}

#[derive(Debug)]
pub struct RiddleInput {
  proportion: String,
  value: String,
}
/**
 * This is solving the full riddle given the params.
 * What is 3/7 chicken, 2/3 cat and 2/4 goat?
 */
fn solve_riddle_2(input: Vec<RiddleInput>) -> String {
  let mut result: Vec<&str> = vec![];

  for x in input.iter() {
    let mut proportion_parts: Vec<usize> = vec![];

    // transform the ["3","7"] into the [3,7]
    for part in x.proportion.split('/') {
      proportion_parts.push(part.parse().unwrap());
    }

    // if the second item is not the same as the length of the string we should not continue
    if proportion_parts[1] != x.value.len() {
      panic!("Cannot be computed, denominator doesn't match the value length");
    }

    // take the amount of the characters from the start of the value string
    let value_part = &x.value[..proportion_parts[0]];
    result.push(value_part);
  }

  println!("{}", result.join(""));
  result.join("")
}

fn main() {
  println!("Solving the riddle: If 1/2 of 5 is 3, then what is 1/3 of 10?");
  println!("{}", solve_riddle_1(10, 1.0 / 3.0));
  println!("--------------------------------------------------------------");
  println!("Solving the riddle: What is 3/7 chicken, 2/3 cat and 2/4 goat?");
  // correct solution
  solve_riddle_2(vec![
    RiddleInput {
      value: String::from("chicken"),
      proportion: String::from("3/7"),
    },
    RiddleInput {
      value: String::from("cat"),
      proportion: String::from("2/3"),
    },
    RiddleInput {
      value: String::from("goat"),
      proportion: String::from("2/4"),
    },
  ]);
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn riddle_1_should_pass() {
    println!("Solving the riddle: If 1/2 of 5 is 3, then what is 1/3 of 10?");
    solve_riddle_1(5, 0.5);
    solve_riddle_1(10, 1.0 / 3.0);
  }

  #[test]
  fn riddle_2_should_pass() {
    println!("Solving the riddle: What is 3/7 chicken, 2/3 cat and 2/4 goat?");

    solve_riddle_2(vec![
      RiddleInput {
        value: String::from("chicken"),
        proportion: String::from("3/7"),
      },
      RiddleInput {
        value: String::from("cat"),
        proportion: String::from("2/3"),
      },
      RiddleInput {
        value: String::from("goat"),
        proportion: String::from("2/4"),
      },
    ]);
  }
  #[test]
  #[should_panic]
  fn riddle_2_should_fail() {
    println!("Solving the riddle: What is 3/7 chicken, 2/3 cat and 2/4 goat?");

    solve_riddle_2(vec![RiddleInput {
      value: String::from("chicken"),
      proportion: String::from("3/9"),
    }]);
  }
}
